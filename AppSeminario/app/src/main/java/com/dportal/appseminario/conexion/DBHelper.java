package com.dportal.appseminario.conexion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dportal.appseminario.util.Constantes;

public class DBHelper extends SQLiteOpenHelper {
    public static final String TAG="DBHelper";
    /*
    public DBHelper(@Nullable Context context,@Nullable String name,@Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    */

    public DBHelper(Context context) {
        super(context, Constantes.DB_NAME, null, Constantes.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,">>> Em el metodo oncreate()");

        if (db!=null) {
            db.execSQL(Constantes.SCRIPT_CREATE);
            Log.d(TAG,">>> Tabla Creadad correctamente");
        }else{
            Log.d(TAG,">>> Base de datos NULL");

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionInicial, int versionFinal) {
        if(versionFinal>versionInicial){
            db.execSQL(Constantes.SCRIPT_DROP);
            onCreate(db);
        }

    }
}
