package com.dportal.appseminario.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dportal.appseminario.R;

import java.util.Date;

public class InicioSeminarioActivity extends AppCompatActivity {
    public static final String TAG="InicioSeminarioActivity";


    EditText edtIdAsis,edtNombreAsis,edtApellidoAsis,edtFecNacAsis,edtDniAsis,edtSexoAsis,edtEmailAsis,edtTelefonoAsis;
    Button btnActualizar,btnNuevo,btnEliminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_seminario);

        this.initObjets();//
    }

    protected void initObjets(){
        edtIdAsis = findViewById(R.id.edtIdAsis);
        edtNombreAsis = findViewById(R.id.edtNombreAsis);
        edtApellidoAsis = findViewById(R.id.edtApellidoAsis);
        edtFecNacAsis = findViewById(R.id.edtFecNacAsis);
        edtDniAsis = findViewById(R.id.edtDniAsis);
        edtSexoAsis = findViewById(R.id.edtSexoAsis);
        edtEmailAsis = findViewById(R.id.edtEmailAsis);
        edtTelefonoAsis = findViewById(R.id.edtTelefonoAsis);

        btnNuevo = findViewById(R.id.btnNuevo);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnEliminar = findViewById(R.id.btnEliminar);

    }
    public void onClickNuevo(View view){
        Log.d(TAG,">>>INGRESO A METDO ONCLICKNUEVO");
        Intent intent = new Intent(this,RegistroSeminarioActivity.class);
        startActivity(intent);

    }
    public void onClickActualizar(View view){
        Log.d(TAG,">>>INGRESO A METDO ONCLICKNUEVO");

    }
    public void onclickEliminar(View view){
        Log.d(TAG,">>>INGRESO A METDO ONCLICKNUEVO");

    }
}
