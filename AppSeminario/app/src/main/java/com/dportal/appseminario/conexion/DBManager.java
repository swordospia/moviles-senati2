package com.dportal.appseminario.conexion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dportal.appseminario.modelo.Asistente;
import com.dportal.appseminario.util.AsistenteUtil;
import com.dportal.appseminario.util.Constantes;

public class DBManager {
    public static final String TAG="DBManager";

    DBHelper helper = null;
    SQLiteDatabase db = null;

    public DBManager(Context context) {
        helper = new DBHelper(context);
        db = helper.getWritableDatabase();//SE CREA LA BASE DE DATOS PARA ESCRITURAS
    }

    public long insertarAsistente(Asistente asistente){
        Log.d(TAG,">>METODO insertarAsistente");
        long result = db.insert(Constantes.TABLE_NAME,null, AsistenteUtil.getValoresAsistente(asistente));

        if(result !=-1){
            Log.d(TAG,">>Se ha insertado correcto");
        }else{
            Log.d(TAG,">>Error de insercion");
        }

        return result;
    }
    public void eliminarAsistente(int idAsis){
        Log.d(TAG,">>METODO eliminarAsistente");

    }
    public void listarAsistente(){
        Log.d(TAG,">>METODO listarAsistente");

    }
    public void actualizarAsistente(Asistente asistente){
        Log.d(TAG,">>METODO actualizarAsistente");

    }
}
