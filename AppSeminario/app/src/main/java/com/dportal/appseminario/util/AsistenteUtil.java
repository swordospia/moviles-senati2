package com.dportal.appseminario.util;

import android.content.ContentValues;

import com.dportal.appseminario.modelo.Asistente;

public class AsistenteUtil {
    public static ContentValues getValoresAsistente(Asistente asistente){
        ContentValues values = new ContentValues();
        values.put(Constantes.COLUMN_NOMBRES,asistente.getNombreAsis());
        values.put(Constantes.COLUMN_APELLIDOS,asistente.getApellidoAsis());
        values.put(Constantes.COLUMN_FEC_NAC,(asistente.getFecNacAsis())==null?"":asistente.getFecNacAsis().toString());
        values.put(Constantes.COLUMN_DNI,asistente.getDniAsis());
        values.put(Constantes.COLUMN_SEXO,asistente.getSexoAsis());
        values.put(Constantes.COLUMN_EMAIL,asistente.getEmailAsis());
        values.put(Constantes.COLUMN_TELEFONO,asistente.getTelefonoAsis());

        return values;
    }
}
