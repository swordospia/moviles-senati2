package com.dportal.appseminario.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dportal.appseminario.R;
import com.dportal.appseminario.conexion.DBManager;
import com.dportal.appseminario.modelo.Asistente;

public class RegistroSeminarioActivity extends AppCompatActivity {
    public static final String TAG="RegisSeminarioActivity";

    EditText edtIdAsisReg,edtNombreAsisReg,edtApellidoAsisReg,edtFecNacAsisReg,edtDniAsisReg,edtSexoAsisReg,edtEmailAsisReg,edtTelefonoAsisReg;
    Button btnRegistrar,btnSalir;

    DBManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_seminario);
        this.initObjetsReg();

        manager = new DBManager(this);
    }

    public void onClickRegistrar(View view){
        Log.d(TAG,">>>Metodo onClickRegistrar");

        Asistente asistente = new Asistente();

        //asistente.setId(edtIdAsisReg.getId());
        asistente.setNombreAsis(edtNombreAsisReg.getText().toString());
        asistente.setApellidoAsis(edtApellidoAsisReg.getText().toString());
        //asistente.setFecNacAsis(edtFecNacAsisReg.getText().toString());
        asistente.setDniAsis(edtDniAsisReg.getText().toString());
        asistente.setSexoAsis(edtSexoAsisReg.getText().toString());
        asistente.setEmailAsis(edtEmailAsisReg.getText().toString());
        asistente.setTelefonoAsis(edtTelefonoAsisReg.getText().toString());

        long result = manager.insertarAsistente(asistente);

        if (result !=-1){
            String mensaje ="Registro: "+result+" insertado correctamente";
            Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Error de insercion. ", Toast.LENGTH_SHORT).show();
        }
    }

    protected void initObjetsReg(){
        edtIdAsisReg = findViewById(R.id.edtIdAsisReg);
        edtNombreAsisReg = findViewById(R.id.edtNombreAsisReg);
        edtApellidoAsisReg = findViewById(R.id.edtApellidoAsisReg);
        edtFecNacAsisReg = findViewById(R.id.edtFecNacAsisReg);
        edtDniAsisReg = findViewById(R.id.edtDniAsisReg);
        edtSexoAsisReg = findViewById(R.id.edtSexoAsisReg);
        edtEmailAsisReg = findViewById(R.id.edtEmailAsisReg);
        edtTelefonoAsisReg = findViewById(R.id.edtTelefonoAsisReg);

        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnSalir = findViewById(R.id.btnSalir);

    }
    public void onClickSalir(View view){
        Log.d(TAG,">>>Metodo onClickSalir");
        Intent intent = new Intent(this,InicioSeminarioActivity.class);
        startActivity(intent);
    }
}
