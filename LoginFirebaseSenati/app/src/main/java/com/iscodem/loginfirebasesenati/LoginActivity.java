package com.iscodem.loginfirebasesenati;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG ="LoginActivity";

    public static final int SIGN_IN_GOOGLE_CODE=1;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private SignInButton btnSignInGoogle;
    private GoogleApiClient googleApiClient;

    EditText edtLogin, edtClave;
    Button btnIngresar, btnNuevoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initObjects();

        initialize();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickIngresar(edtLogin.getText().toString(),edtClave.getText().toString());
                Intent intent = new Intent(getApplicationContext(),BienvenidaActivity.class);
                startActivity(intent);
            }
        });

        btnNuevoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNuevoUsuario(edtLogin.getText().toString(),edtClave.getText().toString());
            }
        });

        btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,SIGN_IN_GOOGLE_CODE);
            }
        });
    }

    private void initialize(){
        Log.d(TAG,">>>Metodo initialize()");

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();

                if(firebaseUser!=null){
                    Log.d(TAG,">>>onAuthStateChanged - SignIn()");
                    Log.d(TAG,">>>UserID - "+firebaseUser.getUid());
                    Log.d(TAG,">>>Email - "+firebaseUser.getEmail());
                }else{
                    Log.d(TAG,">>>onAuthStateChanged - SignOut()");
                }
            }
        };

        Log.d(TAG,">>>Inicio de GoogleSignInOptions()");
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

    }

    private void onClickIngresar(String email, String clave){
        Log.d(TAG,">>>Inicio de onClickIngresar()");
        firebaseAuth.signInWithEmailAndPassword(email,clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Logueado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Falló en inicio de sesión.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onClickNuevoUsuario(String email, String clave){
        Log.d(TAG,">>>Inicio de onClickNuevoUsuario()");
        firebaseAuth.createUserWithEmailAndPassword(email,clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Creado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Falló la cración de usuario.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void SignInGoogleFirebase(GoogleSignInResult googleSignInResult){
        Log.d(TAG,">>>Inicio de SignInGoogleFirebase()");
        if(googleSignInResult.isSuccess()){
            AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInResult.getSignInAccount().getIdToken(),null);
            firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(LoginActivity.this, "Usuario logueado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),BienvenidaActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "Falló el inicio de sesión - signInWithCredential()", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Falló inicio de sesión - googleSignInResult.isSuccess()", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,">>>Inicio de onActivityResult()");
        if(requestCode == SIGN_IN_GOOGLE_CODE){
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            SignInGoogleFirebase(googleSignInResult);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }


    protected void initObjects(){
        edtLogin = findViewById(R.id.edtLogin);
        edtClave = findViewById(R.id.edtClave);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnNuevoUsuario = findViewById(R.id.btnNuevoUsuario);
        btnSignInGoogle = findViewById(R.id.btnSignInGoogle);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
