package com.dportal.apirestretrofit.interfaces;

import com.dportal.apirestretrofit.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonPlaceHolderApi {
    //metodo encargado de obtner la informacion de API REST
    @GET("posts")
    Call<List<Post>> getPost();
}
